pragma solidity ^0.4.0;


interface IERC20Token {

    function balanceOf(address user) public returns(uint256);
}

contract usingTokenContract {
    
    address public tokenContract = 0x07d9e49ea402194bf48a8276dafb16e4ed633317;
    mapping(bytes32 => bool) public administrators;
    address public tokenReceivingAddress = 0x5f558906aec7b38bebba0f67878957c53ed0e0a3;
    
     modifier onlyOwner(){
        address _customerAddress = msg.sender;
        require(administrators[keccak256(_customerAddress)]);
        _;
    }
    
    function usingTokenContract() {
        administrators[0x7f5be223ca67e25627c96e839775b3401c1ba4d617afc27a77a866e071ed401d] = true;
    }

    
    function changeTokenReceivingAddress(address _newAddress) public onlyOwner() {
        
        tokenReceivingAddress = _newAddress;
        
    }
    
 
    function getBalance(address _userAddress) public constant returns(uint256) {
        
        return IERC20Token(tokenContract).balanceOf(_userAddress);
        
    }
}
