


import { default as Web3} from 'web3';
import { default as uuid} from 'uuid-by-string';
import { default as contract } from 'truffle-contract'


import dale_coin_artifacts from '../../build/contracts/usingTokenContract.json'

var daleCoin = contract(dale_coin_artifacts);

let account;

let hisData;

// var adminAddr = "0x3859A2b4EdCe17F724B6d592b11D783038B6A9BF";
// console.log("this is address",adminAddr);


// var keccakvalue = web3.sha3(web3.toHex(adminAddr),{ encoding: 'hex' });
// console.log("this is keccakvalue",keccakvalue);




function convertTokenToEightDecimal (e) {
  return 1e8 * e
}

function convertEightDecimalToToken (e) {
  return e / 1e8
}


window.changeReceivingAddress = function() {
  let newAddress = $("#accountAddress").val();
  console.log(newAddress);
  $("#status" ).empty();

  try {

    daleCoin.deployed().then(function(contractInstance) {

      contractInstance.changeTokenReceivingAddress(newAddress, {from: account}).then(function(result) {
        console.log(result);
        $("#status").html("Updated Successfully");
      });

    });

  } catch (err) {
    console.log(err);
  }
}

// function loadTable(tableId, fields, data) {
//   //$('#' + tableId).empty(); //not really necessary
//   var rows = '';
//   $.each(data, function(index, item) {
//       var row = '<tr>';
//       $.each(fields, function(index, field) {
//           row += '<td>' + item[field+''] + '</td>';
//       });
//       rows += row + '<tr>';
//   });
//   $('#' + tableId + ' tbody').html(rows);
// }


// $('#btn-update').click(function(e) {

//   var data2 = [
//     { field1: 'new value a1', field2: 'new value a2', field3: 'new value a3' },
//     { field1: 'new value b1', field2: 'new value b2', field3: 'new value b3' },
//     { field1: 'new value c1', field2: 'new value c2', field3: 'new value c3' }
//     ];
//   // var data3 = myData;
//   console.log("in function",hisData);

//   loadTable('data-table', ['field1', 'field2', 'field3'], hisData);
// });

// window.setGroupStaking = function() {
//   let tokenNumber = convertTokenToEightDecimal($("#inputMessaging").val());
//   console.log(tokenNumber);
//   let groupName = $("#groupName").val()
//   var groupNameinUuid = uuid(groupName);
//   var groupNamein32format = groupNameinUuid.replace(/-/g, "");

//   try {
//     daleCoin.deployed().then(function(contractInstance) {
//       contractInstance.setGroupStakingRequirement(web3.fromAscii(groupNamein32format),tokenNumber, {from: account}).then(function(result) {
//         $("#groupRequirementStatus").html("Updated Successfully");
//       });
//     });
//   } catch (err) {
//     console.log(err);
//   }
// }

// window.validateForJoining = function() {
//   let checkAddress = $("#addressForJoin").val();

//   try {
//     daleCoin.deployed().then(function(contractInstance) {
//       contractInstance.validateUser(checkAddress, {from: account}).then(function(result) {
 
//         let status;
//         if(result){
//           status = "Eligible";
//         }else{
//           status = "Not Eligible";
//         }
//         $("#validateJoinStatus").html("Given User Is "+status);
//       });
//     });
//   } catch (err) {
//     console.log(err);
//   }
// }


// window.validateForJoiningGroup = function() {
//   console.log("calling validate for joining");
//   let checkAddress = $("#addressForMessaging").val();
//   let groupsName = $("#groupsName").val();
//   let groupNameinUuid = uuid(groupsName);
//   let groupNamein32format = groupNameinUuid.replace(/-/g, "");
//   console.log(checkAddress);
//   console.log(groupsName);
//   console.log(groupNamein32format);
//   console.log(account);

//   try {
//     daleCoin.deployed().then(function(contractInstance) {
//       contractInstance.validateUserForGroup(checkAddress,web3.fromAscii(groupNamein32format), {from: account}).then(function(result) {
//          let status;
//          console.log(result);
//         if(result){
//           status = "Eligible";
//         }else{
//           status = "Not Eligible";
//         }
//         $("#validateGroupJoiningStatus").html("Given User Is "+status);
//       });
//     });
//   } catch (err) {
//     console.log(err);
//   }
// }

// window.createGroup = function() {

//   var chatChannelName = $("#chatGroupName").val();
//   let tokensRequired = $("#tokenRequirement").val();




//   let url = "http://68.183.125.38:8080/api/v1/createGroup";


//   let data = { 
//     "channelName" : chatChannelName
//   }



//   $.post(url,data,function(response){
//     console.log(response);
//     if(response){
      
//       $("#groupCreationStatus").html("Group Created Successfully");

//       let groupNameinUuid = uuid(chatChannelName);
//       let groupNamein32format = groupNameinUuid.replace(/-/g, "");
//       let defaultTokensToJoinGroup  = convertTokenToEightDecimal(tokensRequired); //default requirement to join group set as 10

//       try {
//         daleCoin.deployed().then(function(contractInstance) {
//           contractInstance.setGroupStakingRequirement(web3.fromAscii(groupNamein32format),defaultTokensToJoinGroup, {from: account}).then(function(result) {
//             $("#groupCreationStatus").html("Group Token Requirement set to "+tokensRequired);
//           });
//         });
//       } catch (err) {
//         console.log(err);
//       }
//    }
// });

// }

// window.checkGroupTokenRequirement = function () {

//   var checkingChannelName = $("#checkingGroupName").val();

//   console.log(checkingChannelName);
//   let channelUuid = uuid(checkingChannelName);
//   let channdelUuiIn32format = channelUuid.replace(/-/g, "");

//   try {

//     daleCoin.deployed().then(function(contractInstance) {
//       contractInstance.groupRequirement(web3.fromAscii(channdelUuiIn32format), {from: account}).then(function(result) {
//         console.log(result);
//         $("#group_requirement_status").html("Group "+checkingChannelName+" Token Requirement is  "+convertEightDecimalToToken(result));
//       });
//     });
//   } catch (err) {
//     console.log(err);
//   }



// }


$( document ).ready(function() {
  if (typeof web3 !== 'undefined') {
    console.warn("Using web3 detected from external source like Metamask")
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.warn("No web3 detected. Falling back to http://localhost:8545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask");
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
  }


    web3.eth.getAccounts(function (err, accs) {
    
      if (err != null) {
        alert('There was an error fetching your accounts.')
        return
      }

      if (accs.length === 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.")
        return
      }

      account = accs[0]
 

    })
  

    daleCoin.setProvider(web3.currentProvider);

    function reload() {

      console.log("reload happening");

        $("#stakeStatus").html("");
        $("#messageStakeStatus").html("");
  
   
      daleCoin.deployed().then(function(contractInstance) {

        contractInstance.tokenReceivingAddress({from: account}).then(function(result) {

            $("#presentReceivingAccount").html("Present Receiving Address : "+result);


        });
   
    });

}

    setInterval(reload, 8000);

    // $.ajaxSetup({crossDomain: true,xhrFields: {withCredentials: true}});



    // $.get('https://dalecoinchat.com/api/v1/users/',function(result) { 
 
    //          console.log(result);
 
    //          if(result.length) {
 
    //            let tableData = [];
 
    //            for(let n=0; n<result.length; n++) {
   
    //              tableData.push({ field1: result[n].username, field2: result[n].address, field3: result[n].tokenbalance })
                 
    //            }
   
    //            hisData = tableData;
 
       
 
    //          } else {
 
    //           console.log("no users in system");
 
    //          }
           
                 
 
    //          });
});





