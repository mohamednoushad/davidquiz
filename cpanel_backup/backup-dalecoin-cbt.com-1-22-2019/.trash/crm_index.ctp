<?php echo $this->Session->flash();

//App::import('Vendor', 'app.js');?>
<div class="page-title-breadcrumb">
    <div class="page-header pull-left">
	<div class="page-title"><?php echo __('Payment');?></div>
    </div>
</div>
<div class="panel">
    <div class="panel-body">
	<?php echo $this->Form->create('Payment', array('action'=>'checkout','name'=>'post_req','id'=>'post_req','class'=>'form-horizontal'));?>
	     <div class="form-group">
		<label for="group_name" class="col-sm-3 control-label"><?php echo __('Amount');?></label>
		<div class="col-sm-9">
		   <?php echo $this->Form->input('amount',array('id'=>'amount', 'label' => false,'class'=>'form-control','placeholder'=>__('Amount'),'div'=>false));
echo $amount; ?>
		<label id="dalecoin" />
        </div>
        
	    </div>
	    <div class="form-group">
		<label for="group_name" class="col-sm-3 control-label"><?php echo __('Remarks');?></label>
		<div class="col-sm-9">
		   <?php echo $this->Form->input('remarks',array('id' => 'remarks','label' => false,'class'=>'form-control','placeholder'=>__('Remarks'),'div'=>false));?>
		</div>
	    </div>                    
	    <div class="form-group text-left">
		<div class="col-sm-offset-3 col-sm-7">
		    <button type="button"  onclick="addMoney()"class="btn btn-success"><span class="fa fa-paypal"></span> <?php echo __('Pay ');?></button>
		</div>
	    </div>
	<?php echo $this->Form->end();?>
    </div>
</div>
<script>

let receivingAddress;
let daleCoinUnitPrice;
let userTokenBalance;

let web3js;

let tokenContractAbi = [{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"version","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"},{"name":"_extraData","type":"bytes"}],"name":"approveAndCall","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[{"name":"_initialAmount","type":"uint256"},{"name":"_tokenName","type":"string"},{"name":"_decimalUnits","type":"uint8"},{"name":"_tokenSymbol","type":"string"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"payable":false,"stateMutability":"nonpayable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"}];

let mainContractAbi = [{"constant":true,"inputs":[{"name":"","type":"bytes32"}],"name":"administrators","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"tokenReceivingAddress","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"tokenContract","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_newAddress","type":"address"}],"name":"changeTokenReceivingAddress","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_userAddress","type":"address"}],"name":"getBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"}];

let mainContract;
let mainContractInstance;

let tokenContract;
let tokenContractInstance;


let mainContractAddress = "0x48a741d0c13cd6504654c7244b6eee58f6d76515"; 
let tokenContractAddress = "0x07D9e49Ea402194bf48A8276dAfB16E4eD633317";

function convertTokenToEightDecimal (e) {
    return 1e8 * e
  }
  
function convertEightDecimalToToken (e) {
    return e / 1e8
  }

function getReceipt(txHash, interval) {
   
    const transactionReceiptAsync = function(resolve, reject) {
        web3.eth.getTransactionReceipt(txHash, (error, receipt) => {
            if (error) {
                reject(error);
            } else if (receipt == null) {
                setTimeout(
                    () => transactionReceiptAsync(resolve, reject),
                    interval ? interval : 500);
            } else {
                resolve(receipt);
            }
        });
    };

    if (Array.isArray(txHash)) {
        return Promise.all(txHash.map(
            oneTxHash => web3.eth.getTransactionReceiptMined(oneTxHash, interval)));
    } else if (typeof txHash === "string") {
        return new Promise(transactionReceiptAsync);
    } else {
        throw new Error("Invalid Type: " + txHash);
    }
};

function addMoney()  {

    let amountInserted = $("#amount").val();
    let daleCoinsToSpend = amountInserted / daleCoinUnitPrice;
    
    try {

        if(daleCoinsToSpend<userTokenBalance) {
            

            tokenContractInstance.transfer(receivingAddress,convertTokenToEightDecimal(daleCoinsToSpend),{to:tokenContractAddress, from:web3.eth.accounts[0]},function (error, result) {

   
                    if(!error) {

                    return getReceipt(result).then(function (receipt) {

                        console.log(receipt);
            
                        if(receipt.status = "0x1") {

                
               
                var student_id = '<?php echo $_SESSION["stu_id"]?>'
                $.post( "http://dalecoin-cbt.com/addcoins/addmoney.php",{"amt":amountInserted,"student_id":student_id,"remarks":remarks})
  .done(function( data,error ) {
    alert( "Data Loaded: " + data );
    console.log(error);
  });
                
               }else {
                $("#equivalentDaleCoin").html("Sorry, You were not able to Dale Coins");

               }
            });
                    }
                    else {
                        console.error(error);
                    }
            })

    } else {

        $('#dalecoin').html("You donot have enough tokens to make transaction");

    }
  } catch (err) {
      console.log(err);
    }


  }




$('#amount').bind('input',function(){
    let inputDollar = $('#amount').val();
    let daleCoinsToSpend = inputDollar / daleCoinUnitPrice;
    $('#dalecoin').html("One Dale Coin is "+ daleCoinUnitPrice + " and equivalent dale coin to spend is "+daleCoinsToSpend);
});


window.addEventListener('load', function() {

    setTimeout(function() {

    if (typeof web3 !== 'undefined') {
        
        console.log('Web3 Detected! ' + web3.currentProvider.constructor.name)
     
        window.web3 = new Web3(web3.currentProvider);

                
            mainContract = web3.eth.contract(mainContractAbi);
            mainContractInstance = mainContract.at(mainContractAddress);

            mainContractInstance.tokenReceivingAddress(function(error, result){

                if(!error) {
                    receivingAddress = result;
                    console.log(result)
                } else {
                    console.error(error);
                }
                    
            });


            tokenContract = web3.eth.contract(tokenContractAbi);
            tokenContractInstance = tokenContract.at(tokenContractAddress);

            tokenContractInstance.balanceOf(web3.eth.accounts[0],function(error, result){

                userTokenBalance = convertEightDecimalToToken(result.toNumber());

                        
            });

            $.get( "https://api.coingecko.com/api/v3/simple/price?ids=dalecoin&vs_currencies=usd", function( data ) {

                    daleCoinUnitPrice = data.dalecoin.usd;

            });

        

    } else {
        alert('Please connect Ethereum Client !');
    }
     },500);
});
</script>