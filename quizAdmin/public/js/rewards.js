window.setReward = function () {

    $("#inputStatus").empty();

    let position = parseInt($("#positionPlace").val());
    let prizeTokens = $("#rewardToken").val();
    
    if (Number.isInteger(position)) {

        var formData = {
    		place : position,
    		prize :  prizeTokens
    	}
    	

        $.ajax({
			type : "POST",
			contentType : "application/json",
			url : window.location + "addRewards",
			data : JSON.stringify(formData),
            dataType : 'json',
          	success : function(customer) {
				$("#inputStatus").html("Updated Successfully!"); 
			},
			error : function(e) {
				alert("Error!")
				console.log("ERROR: ", e);
			}
		});


        
    }else {
  
        $("#inputStatus").html("Please enter a valid position number");
    }

}