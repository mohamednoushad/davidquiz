var mysql  = require('mysql');

// var connection = mysql.createConnection({
//     host     : '50.116.93.181',
//     user     : 'v67b08fo_noushad',
//     password : 'noushad',
//     database : 'v67b08fo_eduproo'
//   });


// connection.connect();

function handleDisconnect() {
     connection = mysql.createConnection({
        host     : '50.116.93.181',
        user     : 'v67b08fo_noushad',
        password : 'noushad',
        database : 'v67b08fo_eduproo'
      });
    // Recreate the connection, since
                                                    // the old one cannot be reused.
  
    connection.connect(function(err) {              // The server is either down
      if(err) {                                     // or restarting (takes a while sometimes).
        console.log('error when connecting to db:', err);
        setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
      }                                     // to avoid a hot loop, and to allow our node script to
    });                                     // process asynchronous requests in the meantime.
                                            // If you're also serving http, display a 503 error.
    connection.on('error', function(err) {
      console.log('db error', err);
      if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
        handleDisconnect();                         // lost due to either server restart, or a
      } else {                                      // connnection idle timeout (the wait_timeout
        throw err;                                  // server variable configures this)
      }
    });
  }

module.exports = {

    getAllTransactions : async function() {

         var sql = "SELECT * FROM wallets";

        return new Promise((resolve, reject) => {
    
            connection.query(sql,async function(error,result){
       
                if(result){
    
                    resolve (result);
            
                } else {
                    reject (new Error("its error"));
                }
            })
                
        });

    },

    endConnection : async function() {

         return await connection.end();
    },

    updateRewards : async function (position,prizeTokens) {

        var sql = "INSERT INTO rewards (position, reward) VALUES ("+mysql.escape(position)+", "+mysql.escape(prizeTokens)+") ON DUPLICATE KEY UPDATE position="+mysql.escape(position)+", reward="+mysql.escape(prizeTokens);

        return new Promise((resolve, reject) => {
    
            connection.query(sql,async function(error,result){
       
                if(result){
    
                    resolve (result);
            
                } else {
                    reject (new Error("its error"));
                }
            })

        });

    }
}

handleDisconnect();
