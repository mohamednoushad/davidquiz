var express = require('express');
const cors = require('cors')
//var cookieSession = require('cookie-session');
var session = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
const db = require('./mysql');

var app = express();


// define middleware
const middleware = [

cookieParser(),
session({
secret: 'quiz-admin',
key: 'quiz-admin',
resave: false,
saveUninitialized: true,
cookie: { maxAge: 1209600000 } // two weeks in miliseconds
  })
]
app.use(middleware);


app.use(cors({credentials: true, origin: true}))
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.set('view engine', 'ejs')

app.set('json spaces', 2);


async function saveUser(name,account) {

  console.log("saving user in db",name,account);
  try {
    
  let tokensWithUserBN = await validateUser.getUserBalance(account);

  let tokensWithUser = convertEightDecimalToToken(tokensWithUserBN.toNumber());

  let userStatus = await db.checkifUserExist(name);

  if (userStatus) {

    await db.updateUserData(name,account,tokensWithUser);

  } else {

    await db.insertUser(name,account,tokensWithUser);

  }

} catch (e) {
  console.log(e);
}

}



app.get('/transactions',function(req,res){

    res.render('demo');

 });


app.get('/',function(req,res){

  res.render('index');

});






app.get('/users',async function(req,res){



  try {

    let users = await db.getAllTransactions();

   
    res.json(users);

    
  } catch(e) {

    console.log(e);
    res.send(e);

  }


});


app.post('/addRewards',async function(req,res){


  let position = req.body.place;
  let prizeTokens = req.body.prize;

  console.log(position);
  console.log(prizeTokens);


  try {

    var result = await db.updateRewards(position,prizeTokens);

    console.log(result);

 
    if(result){

      res.send({result:"success"});

    }else{

      console.log(result);
      res.send({result});

    }


  }catch(e) {
    console.log(e);
    res.send(e);
  }

 
});




app.listen(4600,function(){
console.log("App Started on PORT 4600");
});

